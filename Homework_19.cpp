﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice() {};
};

class Dog : public Animal 
{
public:
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << endl;
    }
};

class Mouse : public Animal
{
public:
    void Voice() override
    {
        cout << "Cheese or life?" << endl;
    }
};

int main()
{
    Animal* Voices[3];
    Voices[0] = new Dog();
    Voices[1] = new Cat();
    Voices[2] = new Mouse();

    for (Animal* a : Voices)
        a->Voice();

    return 0;
}